const express = require("express");
const router = express.Router();

var net = require("net");

var AllSockets = require("./sockets");

router.get("/:id_enchufe/0", function(req, res) {
  let id_enchufe = req.params.id_enchufe;
  
  // Enchufe que prender (pin) - newStatus
  AllSockets[id_enchufe].write(id_enchufe + "-1");

  res.send("OK");
});

router.get("/:id_enchufe/1", (req, res, next) => {
  let id_enchufe = req.params.id_enchufe;

  AllSockets[id_enchufe].write(id_enchufe + "-0");
  res.send("OK");
});

module.exports = router;
