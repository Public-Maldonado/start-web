const express = require("express");
var net = require('net');
var ip = require('ip');

const app = express();
const puertoWeb = 80;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const selectRouters = require("./api/routes/select");
const updateRouters = require("./api/routes/update");

app.use("/api/select", selectRouters);
app.use("/api/update", updateRouters);

const nodeMcuRouters = require("./nodemcu/nodeMcu");
app.use("/nodemcu", nodeMcuRouters);


app.use(express.static(__dirname));

app.listen(puertoWeb, () => console.log(`App listening on port ${puertoWeb}!`));



const puertoNodemcuServer = 2022;

var AllSockets = require("./nodemcu/sockets");

var server = net.createServer(function (socket) {
  socket.on('data', function (data) {
    // var ip_remota = socket.remoteAddress;
    socketRemoto = socket;

    dataJSON = JSON.parse(String(data));

    if (dataJSON.newStatus) {
      console.log("Hay un mensaje");
    }

    AllSockets[dataJSON.id] = socket;

    console.log("Mensaje:   " + String(data));

  });
  socket.on('close', function (hadError) {
    socketRemoto = undefined;
    var ip_remota = socket.remoteAddress;
    if (ip_remota != undefined) {
      console.log("Desconexión de: " + ip_remota);
    }
  });
});

server.listen(puertoNodemcuServer, ip.address);