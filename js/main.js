$(document).ready(function () {

  if (!localStorage.getItem("USER_ID") || !localStorage.getItem("IP_LOCAL")) {
    location.href = "../index.html";
  }

  // Si el archivo actual es "main.html" pido la ubicación, sino no
  if (document.location.href.match(/[^\/]+$/)[0] == "main.html") {
    getWeather();
  };
  $('#content-sidebar').load('../models/sidebar.html');
});


$("#button-hamburger").click(() => {
  $('#sidebar_one').sidebar('toggle');
  $('#sidebar_one, #ui.visible.uncover.sidebar, .pusher, .ui.visible.left.sidebar ~ .fixed, .ui.visible.left.sidebar ~ .pusher').addClass("background-gradient")
});

$("#logout").click((e) => {
  e.preventDefault();
  localStorage.removeItem("IP_LOCAL");
  localStorage.removeItem("USER_ID");
  location.href = "../index.html";
});

// Weather API
function getWeather() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
      var long = position.coords.longitude;
      var lat = position.coords.latitude;
      var api = 'https://fcc-weather-api.glitch.me/api/current?lat=' + lat + '&lon=' + long + '';
      console.log(api)
      $.ajax({
        url: api, success: function (result) {
          currentTempInCelsius = Math.round(result.main.temp);
          //result.name is a barrio and result.sys.country is the coutry
          $('#weather-grados').html(currentTempInCelsius + " °C");
          $('#weather-city').html(result.name);
          // result.weather[0].main is the state of weather (ex: clouds, rain, snow, clear, etc.)
          var stateWeather = result.weather[0].main;
          setIconWeather(stateWeather);

        }
      });
    });

  } else {
    console.log("Geolocation is not supported by this browser. ");
  };
}

function setIconWeather(state) {
  state = state.toLowerCase()
  var classForWeatherIcon;
  switch (state) {
    case 'drizzle':
      classForWeatherIcon = 'wi-day-showers'
      break;
    case 'clouds':
      classForWeatherIcon = 'wi-day-cloudy'
      break;
    case 'rain':
      classForWeatherIcon = 'wi-night-alt-rain-mix'
      break;
    case 'snow':
      classForWeatherIcon = 'wi-day-snow'
      break;
    case 'clear':
      classForWeatherIcon = 'wi-day-sunny'
      break;
    case 'thunderstom':
      classForWeatherIcon = 'wi-day-snow-thunderstorm'
      break;
    default:
      $('div.clouds').removeClass('hide');
  }
  $('#weather-icon').addClass('wi ' + classForWeatherIcon);
}

// Angular
var app = angular.module('myApp', []);

function addCheckBox() {
  //For each card do:
  $('.card-enchufe').each(function (i, obj) {
    // i is a index
    //obj is a div with class "card-enchufe"
    card_div = obj;

    //checkbox is the checkbox (ex: #chk-enchufe1, #chk-enchufe2)
    var checkbox = $(card_div).find('.checkbox');
    $(checkbox)
      .checkbox()
      .first().checkbox({
        onChecked: function () {
          let id_enchufe = checkbox.context.id.split("-")[1];

          var api = `http://${localStorage.getItem("IP_LOCAL")}/api/update/`;

          $.post(api, {
            table_name: "enchufe",
            columns_update: [
              {
                column: "estado",
                new_data: "1"
              }
            ],
            condition: `id = ${id_enchufe}`
          });

          $.get(`http://${localStorage.getItem("IP_LOCAL")}/nodemcu/${id_enchufe}/1`)
            .fail(function () {
              span_color.removeClass("olive");
              span_color.addClass("violet");
              span_color.html("ERROR");

              $(`#dimmer-${id_enchufe}`).dimmer({
                closable: false
              })
              $(`#dimmer-${id_enchufe}`).dimmer('show')

              Me.parents('.center').removeClass('loader');
              Me.parents('.center').removeClass('loader').html('Hubo un problema, por favor refresque la página, y si el error persiste contactános');
            });

        },
        onUnchecked: function () {
          let id_enchufe = checkbox.context.id.split("-")[1];

          var api = `http://${localStorage.getItem("IP_LOCAL")}/api/update/`;

          $.post(api, {
            table_name: "enchufe",
            columns_update: [
              {
                column: "estado",
                new_data: "0"
              }
            ],
            condition: `id = ${id_enchufe}`
          });

          $.get(`http://${localStorage.getItem("IP_LOCAL")}/nodemcu/${id_enchufe}/0`);
        }
      });

  });
  console.log($('.card .image'));
  $('.card .image').dimmer({
    on: 'click'
  });
}

app.controller('myCtrl', function ($scope, $http, $interval, $sce) {
  $scope.enchufesJSON = "";

  $interval(function () {
    $http.get(`http://${localStorage.getItem("IP_LOCAL")}/api/select/enchufe/id_cliente = ${localStorage.getItem("USER_ID")}`)
      .then(function (res) {
        if (angular.toJson(res.data[1].recordset) != $scope.enchufesJSON) {
          $scope.enchufes = res.data[1].recordset;
          $scope.enchufesJSON = angular.toJson(res.data[1].recordset);
          console.log($scope.enchufes);

          setTimeout(() => {
            addCheckBox();
          }, 100);
        }
      });
  }, 2000);
});

// Para emojis - https://stackoverflow.com/questions/22994026/emoji-and-other-unicode-characters-not-appearing-correctly-in-angular
app.filter('unsafeLinky', function ($sce) {
  // Regex from https://github.com/angular/angular.js/blob/master/src/ngSanitize/filter/linky.js#L5
  var urlRegex = /(((ftp|https?):\/\/|(mailto:)?[A-Za-z0-9._%+-]+@)\S*[^\s.;,(){}<>])/gi;
  return function (input, target) {
    var targetHTML = target ? ' target="' + target + '"' : '';
    return $sce.trustAsHtml(input.replace(urlRegex, '<a href="$1"' + targetHTML + '>$1</a>'));
  }
});
