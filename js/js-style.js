
$(document).ready(function () {

  $("#shape-error-login").shape();

  $("#shape-login").shape();

  $("#select-dropdown-gender").dropdown();

  $("#button-send").hide();

  if (localStorage.getItem("USER_ID") && localStorage.getItem("IP_LOCAL")) {
    location.href = "./pages/main.html";
  };

  var DatePrevious = Date.now();
  var DateExecuted = DatePrevious;

  getIP();

});

function getIP() {
  setTimeout(function () {
    let IP = $('#ip').text()
    // console.log(IP);

    if (IP != "LU") {
      localStorage.setItem("IP_LOCAL", IP);;
    }
  }, 0500);
}

//When user click in "Logueate"
$("#send-id-kit").click(() => {
  validateLogin();
});

//When user click in the first "Vamos" (inside modal)
$("#button-flip").on("click", function (e) {
  $("#button-flip").remove();

  $("#shape-login").shape("flip right");

  $("#modal-init")
    .parent()
    .addClass("scroll");

  $("#button-send").show();
});

$("#button-send").click(() => {
  validateInfoAndCard();
});

$(".button-submit").keypress(function (event) {
  if (event.which == 13) {
    //If your key is enter press click in button for login
    $("#send-id-kit").trigger("click");
  }
});

function validateLogin() {
  var user_mail = $("#input-user-mail").val();
  var user_password = $("#input-user-password").val();

  if (user_mail == "") {
    $("#input-user-mail").removeClass("border-color-grey");
    $("#input-user-mail").addClass("border-error");

    $("#input-user-mail").attr("placeholder", "Poné tu ID!!!");

    $("#input-user-mail").transition("jiggle");
  } else if (user_password == "") {
    $("#input-user-password").removeClass("border-color-grey");
    $("#input-user-password").addClass("border-error");

    $("#input-user-password").attr("placeholder", "Poné tu contraseña!!!");

    $("#input-user-password").transition("jiggle");
  } else {
    // Auth to login

    if (!localStorage.getItem("IP_LOCAL")) {
      alert("Hubo un error :C . Por favor refresque la página. De no ser así esta página no funcionará")
    }

    // SELECT C.id FROM cliente C INNER JOIN personas P ON C.id = P.id WHERE P.mail = 'alberto@mail.com' AND P.contrasenia = 'turquesa';
    var api = `http://${localStorage.getItem("IP_LOCAL")}/api/select/innerjoin/`;
    console.log(api);

    var posting = $.post(api, {
      table_name: "cliente",
      table_to_join: "personas",
      columns_to_show: "cliente.id",
      id_this_table: "id",
      id_that_table: "id",
      condition: `personas.mail='${user_mail}' AND personas.contrasenia='${user_password}'`
    });

    posting.done(function (result) {
      console.log(result);
      if (!result[0]) {
        //Error when not log
        $("#input-user-mail").removeClass("border-color-grey");
        $("#input-user-mail").addClass("border-error");

        $("#input-user-password").removeClass("border-color-grey");
        $("#input-user-password").addClass("border-error");

        $("#input-user-mail").transition("jiggle");

        $("#input-user-password").transition("jiggle");

        $("#shape-error-login").shape("flip right");

        // $('#send-id-kit').off("click");
        $("#send-id-kit").off("click");

        console.log("Usted puso un ID de kit inválido");
        setTimeout(function () {
          $("#shape-error-login").shape("flip right");
          $("#send-id-kit").on("click", () => {
            validateLogin();
          });
        }, 4000);
      } else {

        var id_user = result[1].recordset[0].id

        localStorage.setItem("USER_ID", id_user);

        validateInfoAndCard();

        // $(".ui.basic.modal").modal("show");

        // $(".ui.basic.modal")
        //   .modal({
        //     closable: false,
        //     onApprove: function() {
        //       // $('#shape-login').shape('flip right');
        //       return false;
        //     }
        //   })
        //   .modal("show");
        // $("#input-user-mail").removeClass("border-error");
        // $("#input-user-mail").addClass("border-color-grey");

        // $("#input-user-password").removeClass("border-error");
        // $("#input-user-password").addClass("border-color-grey");

        // var today = new Date();

        // // Start calendar - https://github.com/mdehoog/Semantic-UI-Calendar
        // $("#calendarBirthday").calendar({
        //   startMode: "year",
        //   type: "date",
        //   minDate: new Date(
        //     today.getFullYear() - 200,
        //     today.getMonth(),
        //     today.getDate()
        //   ),
        //   maxDate: new Date(
        //     today.getFullYear() - 5,
        //     today.getMonth(),
        //     today.getDate()
        //   ),
        //   initialDate: new Date(
        //     today.getFullYear() - 18,
        //     today.getMonth(),
        //     today.getDate()
        //   ),
        //   formatter: {
        //     date: function(date, settings) {
        //       if (!date) return "";
        //       var day = date.getDate();
        //       var month = date.getMonth() + 1;
        //       var year = date.getFullYear();
        //       return day + "/" + month + "/" + year;
        //     }
        //   }
        // });
      }
    });
  }
}

function validateInfoAndCard() {
  //TODO: validar información

  location.href = "./pages/start.html";
}
