var ctx = $("#myChart");

// Font color
Chart.defaults.global.defaultFontColor = 'black';

// Create Chart
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["12:00", "12:10", "12:20", "12:40", "13:00", "13:40", "14:00", "14:20", "14:40", "15:00", "15:20", "15:40", "16:00", "16:40", "17:00", "17:40", "18:00", "18:20", "18:40", "19:00"],
        datasets: [
            {
                label: 'kW usados en total',
                data: [12, 19, 3, 5, 2, 3, 200, 600, 120, 0, 1200, 520, 420, 320, 220, 222, 222, 420, 420, 1024],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)'
                ],

                //If i don't want to fill the chart:
                //fill: false,
                pointBackgroundColor: 'rgba(255, 99, 132, 0.82)',
                pointBorderColor: 'rgba(255,99,132,1)',
                pointRadius: 5,
                pointHoverRadius: 5.5,
                borderWidth: 1.5
            },
            {
                label: 'Lavarropa',
                data: [600, 520, 5, 420, 420, 420, 320, 3, 3, 222, 222, 220, 200, 2, 19, 1200, 120, 12, 1024, 0],
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                ],

                //If i don't want to fill the chart:
                //fill: false,
                pointBackgroundColor: 'rgba(54, 162, 235, 0.82)',
                pointBorderColor: 'rgba(54, 162, 235, 1)',
                pointRadius: 5,
                pointHoverRadius: 5.5,
                borderWidth: 1.5,
                hidden: true
            },
            {
                label: 'Lámpara mesa de luz',
                data: [320, 12, 222, 1024, 1200, 420, 200, 3, 2, 5, 222, 420, 420, 120, 220, 600, 0, 520, 3, 19],
                backgroundColor: [
                    'rgba(255, 206, 86, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 206, 86, 1)',
                ],

                //If i don't want to fill the chart:
                //fill: false,
                pointBackgroundColor: 'rgba(255, 206, 86, 0.82)',
                pointBorderColor: 'rgba(255, 206, 86, 1)',
                pointRadius: 5,
                pointHoverRadius: 5.5,
                borderWidth: 1.5,
                hidden: true
            }
        ]
    },
    options: {
        tooltips: {
            callbacks: {
                label: function (tooltipItem) {
                    var hora = tooltipItem.xLabel
                    var consumoKw = Number(tooltipItem.yLabel)
                    return "Usaste " + consumoKw + "kw a las " + hora + "hs";
                }
            }
        }

    }
});