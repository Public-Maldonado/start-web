$(document).ready(function () {
  if (!localStorage.getItem("USER_ID") || !localStorage.getItem("IP_LOCAL")) {
    location.href = "/";
  }

  loadEnchufes();
});

// Mensaje de error cuando intenta añadir un enchufe incorrecto
$('.message .close')
  .on('click', function () {
    $(this)
      .closest('.message')
      .transition('fade')
      ;
  })
  ;

// Modal para elegir icono
$("#modal-select-icon").modal({
  onDeny: function () {
    if (!$("#divider").attr("id_image_to_edit")) { // Si no hay nada que editar
      $("#content-icons-select").attr("data_src", "027-socket");
      add_enchufe();
    } else {
      edit_view_new_icon();
    }
  },
  onHide: function () {
    if (!$("#divider").attr("id_image_to_edit")) { // Si no hay nada que editar
      if ($("#content-icons-select").attr("data_src") == "incomplete") { // Si para un nuevo enchufe no se eligió ícono
        $("#content-icons-select").attr("data_src", "027-socket");
        add_enchufe();
      }
    } else {
      edit_view_new_icon();
    }
  }
})

// Modal para agregar un enchufe
$("#button-send-enchufe").click(function () {
  $("#modal-add-enchufe")
    .modal({
      onApprove: function () {
        var id_enchufe = $("#put-id").val();
        var name_enchufe = $('#put-name-enchufe').val();
        var password_enchufe = $('#put-password-enchufe').val();

        if (id_enchufe == "") {
          console.log("NO ID");

          // $("#div-input-name").addClass("error");
          // $("#div-input-name").transition("jiggle");

          return false;
        }

        if (id_enchufe == "") {
          console.log("No puso ID");
          return false;
        };

        if (name_enchufe == "") {
          console.log("No puso nombre de enchufe");
          return false;
        };

        if (password_enchufe == "") {
          console.log("No puso nombre contraseña");
          return false;
        };

        //SELECT * FROM enchufe E INNER JOIN aparato A ON A.id = E.id

        var api = `http://${localStorage.getItem("IP_LOCAL")}/api/select/enchufe/id_cliente IS NULL AND contrasenia='${password_enchufe}' AND id=${id_enchufe}`;

        $.get(api, function (data, status) {
          console.log(data);
          if (data[0]) {
            $("#put-id").attr("data_state", id_enchufe);
            $("#put-name-enchufe").attr("data_state", name_enchufe);
            $("#put-password-enchufe").attr("data_state", password_enchufe);

            $("#modal-select-icon").modal("show");
          } else {
            $('#alert-message-notenchufe').removeClass("hidden");
            console.log("No hay nadie gil");
          }
        });
      }
    })
    .modal("show");
});

$(".inputs-add-enchufe").keypress(function (event) {
  if (event.which == 13) {
    $("#div-add-enchufe").trigger("click");
  }
});

// Configuración para vaciar datos
$("#content-icons-select").attr("data_state", "incomplete");
$("#put-id").attr("data_state", "incomplete");
$("#put-name-enchufe").attr("data_state", "incomplete");
$("#put-password-enchufe").attr("data_state", "incomplete");

//  Cargar todos los enchufes en la vista
function loadEnchufes() {
  var api = `http://${localStorage.getItem("IP_LOCAL")}/api/select/enchufe/id_cliente = ${localStorage.getItem("USER_ID")}`;

  $.get(api, function (data, status) {
    if (data[0]) {
      data[1].recordset.forEach(element => {
        $("#cards-enchufes").append(`
              <div id="card-enchufe-${element.id}" class="ui card card-enchufe">
              <div class="image">
                <img src="../img/house-icons/svg/${element.icono}.svg">
              </div>
              <div id="content-${element.id}" class="content">
                  <a id="header-${element.id}" class="header">${element.nombre}</a>
                  <div class="meta">
                      <span class="date">Enchufe #${element.id}</span>
                  </div>
                  <div class="description user-select-none">
                      ${element.resumen}
                  </div>
              </div>
              <div class="extra content">
                  <div class="statistic display-flex">
                      <div class="value value-cards">
                          <i class="chart area icon"></i> 0 kW
                      </div>
                      <button id="edit-button-${element.id}" data="${element.id}" onclick="editInfoEnchufe(this, ${element.id}, '${element.icono}')" class="mdl-button mdl-js-button mdl-button--icon mdl-js-ripple-effect mdl-button--colored margin-lef-auto display-inline-block">
                        <i id="icon-edit-${element.id}" class="material-icons">edit</i>
                      </button>
                  </div>
              </div>
          </div>
              `);
      });
      console.log(data);
    }
  });
}

// Se modifican los campos para poder editar un enchufe (a su vez también edita el enchufe)
function editInfoEnchufe(element, id, path_image) {

  var name = $(`#header-${id}`).text();

  //Quiere editar
  if ($(`#icon-edit-${id}`).html() == "edit") {

    // -- Input material design
    var $div = $(`
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label textfield-demo margin-top--10">
        <input class="mdl-textfield__input" type="text" id="input-name-${id}"/>
        <label class="mdl-textfield__label" for="input-name-${id}">${name}</label>
      </div>`);

    // Tell the material library about the new elements
    componentHandler.upgradeElement($div[0]);

    $(`#content-${id}`).prepend($div);

    $(`#content-${id} .meta`).addClass("margin-top--5");
    $(`#header-${id}`).addClass("none");

    $(`#icon-edit-${id}`).html("done");


    // -- Dimmer card Semantic
    var $dimmer = $(`
    <div id="dimmer-${id}" class="ui dimmer">
      <div class="content">
        <div class="center">
          <div id="edit-image-${id}" class="button-edit-image ui inverted button"><i class="image icon"></i>Cambiar la imagen</div>
        </div>
      </div>
    </div>`);

    componentHandler.upgradeElement($dimmer[0]);

    $(`#card-enchufe-${id} .image`).prepend($dimmer);

    $(`#dimmer-${id}`).dimmer({
      on: 'click'
    });

    $(`#dimmer-${id}`).dimmer("show");

    $(`#edit-image-${id}`).click(event => {
      $("#divider").attr("id_image_to_edit", id);
      $("#modal-select-icon").modal("show");
    })

    $('.mdl-textfield__input').keypress(function (event) {
      if (event.which == 13) {

        let id_enchufe = $(this).attr("id").split("-")[2];
        $(`#edit-button-${id_enchufe}`).trigger("click");

      }
    });

  } else { // Los cambios ya están hechos jeje

    var newNameEnchufe = $(`#input-name-${id}`).val();

    var query_string;

    if (newNameEnchufe != name && newNameEnchufe) {
      query_string = `UPDATE enchufe SET nombre = '${newNameEnchufe}'`

      name = newNameEnchufe;
      $(`#header-${id}`).html(name);
    };
    if ($("#divider").attr("new_image")) {
      var newImage = $("#divider").attr("new_image");
      if(query_string){
        query_string += `, icono = '${newImage}'`;
      }else{
        query_string = `UPDATE enchufe SET icono = '${newImage}'`
      }
      
      path_image = newImage;
      $(`#card-enchufe-${id} .image img`).attr("src", `../img/house-icons/svg/${path_image}.svg`);
    };

    if (query_string) {
      query_string += ` WHERE id = ${id}`;
      console.log(query_string);
      
      var api = `http://${localStorage.getItem("IP_LOCAL")}/api/update/query`;

      var posting = $.post(api, {
        query: query_string
      });

      posting.done(function (data) {
        console.log("Se cambiaron los datos");
        console.log(data);
      })
    }

    $(`#icon-edit-${id}`).html("edit");

    $(`#input-name-${id}`).parent().remove();

    $(`#content-${id} .meta`).removeClass("margin-top--5");

    $(`#header-${id}`).removeClass("none");

    $(`#dimmer-${id}`).remove();
    $(`#card-enchufe-${id} .image`).removeClass("dimmable dimmed");

    $("#divider").attr("id_image_to_edit", "");
    $("#divider").attr("new_image", "");
  }
}

// Cambia a la nueva imagen que el usuario eligió
function edit_view_new_icon() {
  var id = $("#divider").attr("id_image_to_edit");
  var image = $("#divider").attr("new_image")
  if (image) {
    $(`#card-enchufe-${id} .image img`).attr("src", `../img/house-icons/svg/${image}.svg`);
  }

}

// Seleciona el src de la imagen que yo cliqueé
$("#content-icons-select img").click(event => {
  var target = $(event.target);
  var path_image = target.attr("src");

  var position_start = path_image.search(/[0-9]+-.+\./g)
  var position_final = path_image.length;

  var name_icon = path_image.slice(position_start, position_final - 4);

  $("#content-icons-select").attr("data_state", "complete");
  $("#content-icons-select").attr("data_src", name_icon);

  if (!$("#divider").attr("id_image_to_edit")) {
    add_enchufe();
  } else {
    // var id_image_to_edit = $("#divider").attr("id_image_to_edit");
    $("#divider").attr("new_image", name_icon);
  }

  $("#modal-select-icon").modal("hide");

});

// Añade el enchufe
function add_enchufe() {

  var name = $("#put-name-enchufe").attr("data_state");
  var numberEnchufe = $("#put-id").attr("data_state");
  var src_icon = $("#content-icons-select").attr("data_src");

  var emojis = ["&#x1F947", "&#x1F4AA", "&#x1F525", "&#x1F680", "&#x1F3AF", "&#x1F60E", "&#x1F91F"]; // &#x1F680 ESO PARA : 🚀 : https://apps.timwhitlock.info/unicode/inspect/hex/1F680
  emoji = emojis[Math.floor(Math.random() * emojis.length)];

  var api = `http://${localStorage.getItem("IP_LOCAL")}/api/update`;

  var posting = $.post(api, {
    table_name: "enchufe",
    columns_update: [
      {
        column: "id_cliente",
        new_data: localStorage.getItem("USER_ID")
      },
      {
        column: "nombre",
        new_data: name
      },
      {
        column: "icono",
        new_data: src_icon
      },
      {
        column: "resumen",
        new_data: `Este artefacto fue ingresado recientemente ${emoji}`
      },
    ],
    condition: `id=${numberEnchufe}`

  });
  console.log("Iegue");
  var enchufe = `
  <div id="card-enchufe-${numberEnchufe}" class="ui card card-enchufe">
      <div class="image">
        <img src="../img/house-icons/svg/${src_icon}.svg">
      </div>
      <div id="content-${numberEnchufe}" class="content">
          <a id="header-${numberEnchufe}" class="header">${name}</a>
          <div class="meta">
              <span class="date">Enchufe #${numberEnchufe}</span>
          </div>
          <div class="description user-select-none">
              Este artefacto fue ingresado recientemente ${emoji}
          </div>
      </div>
      <div class="extra content">
          <div class="statistic display-flex">
              <div class="value value-cards">
                  <i class="chart area icon"></i> 0 kW
              </div>
              <button id="edit-button-${numberEnchufe}" data="${numberEnchufe}" onclick="editInfoEnchufe(this, ${numberEnchufe}, '${src_icon}')" class="mdl-button mdl-js-button mdl-button--icon mdl-js-ripple-effect mdl-button--colored margin-lef-auto display-inline-block">
                <i id="icon-edit-${numberEnchufe}" class="material-icons">edit</i>
              </button>
          </div>
      </div>
    </div>
  `;
  $("#cards-enchufes").prepend(enchufe);
}

// Ir al menú principal al apretar el botón de "Terminar"
$("#button-go-main").click(() => {
  $(location).attr("href", "./main.html");
});

