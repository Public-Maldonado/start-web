const express = require("express");
const router = express.Router();

var MSSql = require("../Query");

// 1) /api/select/[table]/[columns]/[condition]

// 2) SELECT enchufe.id, enchufe.tipo, enchufe.estado, enchufe.icono, enchufe.resumen FROM enchufe INNER JOIN funcionalidad ON enchufe.id_funcionalidad = funcionalidad.id WHERE funcionalidad.id_raspberry = 1;

router.post("/", (req, res, next) => {
	var tableName = req.body.table_name;
	var columnsUpdate = req.body.columns_update;
	var condition = "";
	if (req.body.condition) condition = ` WHERE ${req.body.condition}`;

	MSSql.update({ table: tableName, columnsUpdate: columnsUpdate, condition: condition }).then(result => {
		res.status(200).json(result);
	});
});

router.post("/query", (req, res, next) => {
	var query = req.body.query;


	MSSql.select({ fullQuery: `${query}` }).then(result => {
		res.status(200).json(result);
	});
})
module.exports = router;
