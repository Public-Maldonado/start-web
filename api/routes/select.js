const express = require("express");
const router = express.Router();

var MSSql = require("../Query");

// 1) /api/select/[table]/[columns]/[condition]

// 2) SELECT enchufe.id, enchufe.tipo, enchufe.estado, enchufe.icono, enchufe.resumen FROM enchufe INNER JOIN funcionalidad ON enchufe.id_funcionalidad = funcionalidad.id WHERE funcionalidad.id_raspberry = 1;

router.get("/:tableName/", (req, res, next) => {
	var tableName = req.params.tableName;

	MSSql.select({ table: tableName }).then(result => {
		res.header("Access-Control-Allow-Origin","*");
		res.status(200).json(result);
	});
});

router.get("/:tableName/:condition", (req, res, next) => {
	var tableName = req.params.tableName;
	var where = req.params.condition;

	console.log("Nombre de tabla:" + tableName + " - Condición: " + where);

	MSSql.select({ table: tableName, condition: where }).then(result => {
		res.header("Access-Control-Allow-Origin", "*");
		res.status(200).json(result);
	});
});

router.post("/innerjoin", (req, res, next) => {
	var tableName = req.body.table_name;
	var tableToJoin = req.body.table_to_join;
	var idThisTable = req.body.id_this_table;
	var idThatTable = req.body.id_that_table;
	var condition = req.body.condition;
	var columnsToShow = req.body.columns_to_show;

	var query = `SELECT ${columnsToShow} FROM ${tableName} INNER JOIN ${tableToJoin} ON ${tableName}.${idThisTable} = ${tableToJoin}.${idThatTable} WHERE ${condition}`

	MSSql.select({ fullQuery: `${query}` }).then(result => {
		res.header("Access-Control-Allow-Origin","*");
		res.status(200).json(result);
	});
});

router.post("/query", (req, res, next) => {
	var query = req.body.query;


	MSSql.select({ fullQuery: `${query}` }).then(result => {
		res.header("Access-Control-Allow-Origin","*");
		res.status(200).json(result);
	});
})
module.exports = router;
