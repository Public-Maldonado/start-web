const sql = require("mssql");

// ConfigConn.query = "SELECT * FROM tipo_servicio";

const conn = new sql.ConnectionPool({
  user: "SA",
  password: "Start-Password",
  server: "172.17.0.1",
  database: "startdb"
});

module.exports = {
  select: function ({ columns, table, condition, fullQuery } = {}) {
    return conn
      .connect()
      .then(function () {
        var req = new sql.Request(conn);

        if (!fullQuery) {
          if (!columns) columns = "*";

          var QuerySQL = `SELECT ${columns} FROM ${table}`;

          if (condition) QuerySQL += ` WHERE ${condition}`;
        } else {
          QuerySQL = fullQuery;
        };

        // console.log(QuerySQL);

        return req
          .query(QuerySQL)
          .then(function (recordset) {
            conn.close();
            if (recordset.rowsAffected > 0) {
              return [1, recordset];
            } else {
              return [0, "No se encontraron los datos"];
            }
          })
          .catch(function (err) {
            conn.close();
            return [0, "No se encontraron los datos"];
          });
      })
      .catch(function (err) {
        return {
          message: "ERROR",
          error: err
        }
      });
  },
  update: function ({ columnsUpdate, table, condition, fullQuery } = {}) {
    return conn
      .connect()
      .then(function () {
        var req = new sql.Request(conn);

        if (!fullQuery) {
          var QuerySQL = `UPDATE ${table} SET `;

          columnsUpdate.forEach(element => {
            QuerySQL += `${element.column} = '${element.new_data}', `;
          });

          QuerySQL = QuerySQL.slice(0, (QuerySQL.length - 2));

          QuerySQL += ` ${condition}`;

        } else {
          QuerySQL = fullQuery;
        };

        console.log(QuerySQL);

        return req
          .query(QuerySQL)
          .then(function (recordset) {
            conn.close();
            if (recordset.rowsAffected > 0) {
              return [1, "Se modificó correctamente"];
            } else {
              console.log(recordset);
              return [0, "No se encontraron los datos"]; // No sé qué te devuelve
            }
          })
          .catch(function (err) {
            conn.close();
            return [0, "No se actualizaron los datos D:"];
          });
      })
      .catch(function (err) {
        return [0,
          {
            message: "ERROR",
            error: err
          }]

      });
  }
};
